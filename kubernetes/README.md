# L'orchestration de conteneur avec Kubernetes

## Préambule

- [Le-cloud](./presentation-cloud.md)
- [Présentaiton Kubernetes](./presentation-kube.md)

## TP-cours

Prendre ces notions par la pratique :

- [Construction d'un lab](./lab-kube.md)
- [Tests de déploiement d'application](./kube-deployments.md)
- [Gestion des volumes](./kube-volumes.md)
- [Création d'un Loadbalanceur MetalLB](./kube-metallb.md)
- [Utilisation de helm](./kube-helm-monitoring.md) pour monitorer votre cluster
- [Déploiement d'un Ingress](./kube-ingress.md)

